# import reader and runner from upper scope
try:
    from .. import _surveyutilsRunner
except:
    def _surveyutilsRunner(*args, **kwargs):
        raise Exception("Failed to import runner from upper scope.")
try:
    from .. import _surveyutilsReader
except:
    def _surveyutilsReader(*args, **kwargs):
        raise Exception("Failed to import reader from upper scope.")

from._surveyutils import *
from .run import run