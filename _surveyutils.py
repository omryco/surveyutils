from __future__ import annotations
import itertools
import functools
import numpy as np
import pandas as pd
import os
import logging
import pathlib
import matplotlib as mpl
import matplotlib.pyplot as plt
from . import _surveyutilsReader


__all__ = ["SurveyTensor", "SingleRun"]


def make_hashable(args):
    return tuple([arg if type(arg) != dict else str(arg) for arg in args])


class NoDuplicatedInstances(type):
    """This metaclass makes sure that instances that were initialized using the same __init__ arguments are the same object."""
    
    def __init__(cls, *args, **kwargs):
        super().__init__(*args, **kwargs)
        cls._objects = dict()
    
    def __call__(cls, *args):
        hashable_args = make_hashable(args)
        if hashable_args not in cls._objects:
            cls._objects[hashable_args] = type.__call__(cls, *args)
        return cls._objects[hashable_args]


def memoize(f):
    """Memoize decorator. Cannot be used on functions which have **kwargs as input."""
    memo = dict()
    @functools.wraps(f)
    def f_memoized(*args):
        hashable_args = make_hashable(args)
        if hashable_args not in memo:
            memo[hashable_args] = f(*args)
        return memo[hashable_args]
    return f_memoized


class ObjectWithVariables:
    """This object has the option to handle user-defined variables conveniently."""

    def __init__(self):
        self._vars_ObjectWithVariables = dict()
    
    @property
    def vars(self):
        return self._vars_ObjectWithVariables
    
    @staticmethod
    def synchronize_vars(obj1 : ObjectWithVariables, obj2 : ObjectWithVariables):
        for var in obj1.vars:
            if var in obj2.vars:
                assert obj1.vars[var] == obj2.vars[var], f"Variable {var!r} exists in both objects but does not match!"
        obj1._vars_ObjectWithVariables.update(**obj2._vars_ObjectWithVariables)
        obj2._vars_ObjectWithVariables = obj1._vars_ObjectWithVariables


class ObjectWithConstantPath:
    """this object holds a constant path property"""

    def __init__(self, path):
        self._path = path # never use self._path!!
    
    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, value):
        raise Exception("`path` property cannot be editted!")


class SurveyTensor(ObjectWithVariables, ObjectWithConstantPath, metaclass=NoDuplicatedInstances):

    _allowed_slicing_functions = {min, max}

    def __init__(self, path, parameters : dict):
        ObjectWithVariables.__init__(self)
        ObjectWithConstantPath.__init__(self, path)
        self._parameters = parameters
    
    def __iter__(self):
        for parameter_values in itertools.product(*self._parameters.values()):
            yield SingleRun(self.path, dict(zip(self._parameters.keys(), parameter_values)))

    def slice(self, **kwargs):
        for key in kwargs:
            assert key in self._parameters.keys(), f"Parameter {key!r} does not exist"
            if kwargs[key] not in SurveyTensor._allowed_slicing_functions:
                assert kwargs[key] in self._parameters[key], f"Parameter {key!r} does not have a value {kwargs[key]!r} in this tensor!"
        sliced_dict_ = dict()
        for key in self._parameters:
            if key in kwargs:
                if kwargs[key] in SurveyTensor._allowed_slicing_functions:
                    kwargs[key] = kwargs[key](self._parameters[key])
                sliced_dict_[key] = [kwargs[key]]
            else:
                sliced_dict_[key] = self._parameters[key]
        return SurveyTensor(self.path, sliced_dict_)

    def __getitem__(self, key):
        return self._parameters[key].copy()
    
    def __len__(self):
        len_ = 1
        for key in self.keys():
            len_ *= len(self[key])
        return len_
    
    @property
    def single_run(self):
        assert len(self) == 1, "This tensor has more than a single run!"
        return list(self)[0]
    
    def _repr_html_(self):
        """Beautiful representation in Jupyter notebook"""
        return f"<p>Survey output directory:/p>\n<p>{self.path}</p>" + \
            pd.DataFrame(list(itertools.product(*self._parameters.values())), columns=self.keys())._repr_html_()
    
    def keys(self):
        return self._parameters.keys()
    
    def params(self):
        return list(self.keys())
    
    def free_params(self):
        """Returns the free parameters - Parameters which have more than a single possible value"""
        return [key for key in self.keys() if len(self[key]) != 1]
    
    def det_params(self):
        """Returns the determined parameters - Parameters which have exactly one possible value"""
        return [key for key in self.keys() if len(self[key]) == 1]

    def free_params_dict(self):
        return {parameter : self[parameter] for parameter in self.free_params()}
    
    def det_params_dict(self):
        return {parameter : self[parameter] for parameter in self.det_params()}
    
    def iter_params(self, *parameters):
        """Iterate over values of survey parameters in `parameters`"""
        for parameter in parameters:
            assert parameter in self._parameters, f"{parameter!r} is not a valid parameter name!"
        for values in itertools.product(*[self[parameter] for parameter in parameters]):
            yield dict(zip(parameter, values))
    
    def iter_params_but(self, *parameters):
        """Iterate over values of survey parameters not in `parameters`"""
        for parameter in parameters:
            assert parameter in self._parameters, f"{parameter} is not a valid parameter name"
        return self.iter_params(*[parameter for parameter in self._parameters if parameter not in parameters])

    @staticmethod
    @memoize
    def from_path(survey_output_path):

        # make sure `survey_output_path` is absolute
        survey_output_path = str(pathlib.Path(survey_output_path).absolute())

        # make sure the output folder exists
        assert os.path.isdir(survey_output_path), f"The directory {survey_output_path!r} does not exist"

        # make sure the output folder contains `input_parameters.py`
        input_parameters_path = os.path.join(survey_output_path, "input_parameters.py")
        assert os.path.isfile(input_parameters_path), f"The directory {survey_output_path!r} does not contain an `input_parameters.py` file"

        # get the parameters dictionary from the output path
        with open(input_parameters_path, 'r') as dict_file:
            parameters_dict = eval(dict_file, {'np' : np})

        # sort non-dictionary entries (set, list, np.ndarray). This implicitly casts to list.
        for key in parameters_dict:
            if not isinstance(parameters_dict[key], dict):
                parameters_dict[key] = sorted(parameters_dict[key])
        
        # create a survey tensor 
        survey_tensor = SurveyTensor(survey_output_path, parameters_dict)
        
        return survey_tensor


class SingleRun(ObjectWithVariables, ObjectWithConstantPath, metaclass=NoDuplicatedInstances):

    def __init__(self, path, parameters : dict):
        ObjectWithVariables.__init__(self)
        ObjectWithVariables.synchronize_vars(self, SurveyTensor.from_path(path).slice(**parameters))
        ObjectWithConstantPath.__init__(self, os.path.join(path, SingleRun._config2dirtree(parameters)))
        self._parameters = parameters
    
    @property
    def parameters(self):
        return self._parameters.copy()
    
    @property
    def reader(self):
        if not hasattr(self, "_reader"):
            self._reader = _surveyutilsReader(os.path.join(self.path, "generated_output_file.h5"))
        return self._reader

    def __getitem__(self, key):
        return self._parameters[key]
    
    def __str__(self):
        return self._parameters.__str__()

    def keys(self):
        return self._parameters.keys()
    
    def params(self):
        return list(self.keys())

    @staticmethod
    def _config2dirtree(config : dict):
        dirtree = "outputs"
        for parameter, value in config.items():
            if isinstance(value, str):
                value_to_write = value.replace("/", "_").replace(" ", "_")
            elif isinstance(value, float):
                value_to_write = f"{value:g}"
            else:
                value_to_write = value
            dirtree += f"/{parameter}={value_to_write}"
        return dirtree
        