import argparse
from datetime import datetime
import getpass
import itertools
import pathlib
import git
import re
import os
import sys
from . import run

parser = argparse.ArgumentParser()
parser.add_argument("survey_path", help="The path to the desired survey directory.")
parser.add_argument("config", help="The compilation configuration.")
parser.add_argument("-o", "--output-dir", default="print", choices=['print', 'archive3'], help="The output directory.")
parser.add_argument("-t", "--template", default="template.py", help="The templated output file path.")
parser.add_argument("-p", "--parameters", default="parameters.py", help="The survey parameters dictionary file path.")
parser.add_argument("-e", "--exe-name", default=None, 
                    help="The name of the executable file. If not given, it is inferred from the git repository name.")
parser.add_argument("-b", "--build-dir", default="build", help="Thename of the build directory.")

# default value assumes structure such as mars/utils/surveyutils
parser.add_argument("-w", "--projret-path", default=str(pathlib.Path(__file__).parent.parent.parent.parent.absolute()), 
                    help="The project's path.")

args = parser.parse_args()

# parse project's name using git
project_name = re.search(r'git@gitlab.com:([\x\d]+)/([\x\d]+).git', git.Repo(args.project_path).git.remote("get-url", "origin")).group(2)

# the username which executes the code
username = getpass.getuser()

# get the survey name
try: 
    with open(os.path.join(args.survey_path, "surveyname.txt"), 'r') as f:
        survey_name = f.read()
except FileNotFoundError:
    raise Exception(f"Survey {args.survey_path!r} is missing a 'surveyname.txt' file!")
if not re.match(r'^[\w\d_-]+$', survey_name):
    raise Exception(f"Survey name of survey {args.survey_path!r} contains ileagal characters! Only letters, numbers, '_' ans '-' are allowed.")

time = datetime.now().strftime('%d-%b-%Y-%H-%M-%S')
output_dir = f"/{args.output_dir}/{username}/surveyoutputs/{project_name}/{args.config}/{survey_name}{time}"

if args.exe_name is None:
    exe_name = project_name
else:
    exe_name = args.exe_name

exe_path = os.path.join(args.project_path, args.build_dir, args.config, exe_name)

run(survey_path=args.survey_path,
    output_dir=output_dir,
    template_path=args.template,
    parameters_path=args.parameters,
    exe_path=exe_path)
