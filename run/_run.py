import numpy as np
import sys
import os
import re
import pathlib
import shutil
from .. import jobsmanager, SurveyTensor, _surveyutilsRunner


def run(*, survey_path, output_dir, template_path, parameters_path, exe_path):

    # change paths to absolute and make sure they exist
    survey_path = str(pathlib.Path(survey_path).absolute())
    assert os.path.isdir(survey_path), f"The survey directory {survey_path!r} dose not exist."
    template_path = os.path.join(survey_path, template_path)
    assert os.path.isfile(template_path), f"The template file {template_path!r} does not exist."
    parameters_path = os.path.join(survey_path, parameters_path)
    assert os.path.isfile(parameters_path), f"The parameters file {parameters_path!r} does not exist."
    exe_path = str(pathlib.Path(exe_path).absolute())
    assert os.path.isfile(exe_path), f"The executable {exe_path!r} does not exist."

    # read the input parameters dictionary
    with open(parameters_path, 'r') as dict_file:
        input_parameters_dict = eval(dict_file.read(), {'np' : np})

    # check for duplications
    for key in input_parameters_dict:
        if not isinstance(input_parameters_dict[key], dict):
            assert len(input_parameters_dict[key]) == len(set(input_parameters_dict[key])), f"Input parameter {key!r} has duplications."
    
    # make sure that the template uses all the survey parameters
    with open(template_path, 'r') as f:
        template = f.read()
        for parameter in input_parameters_dict:
            assert re.search(rf"\bSurveyParameters.{parameter}\b", template), f"Template file {template_path!r} does not use the survey parameter {parameter!r}!"
    
    # create output_dir if doesn't exist
    os.makedirs(output_dir, exist_ok=True)

    # copy survey files to output directory. Not preserving original names to create uniformity
    shutil.copyfile(template_path, os.path.join(output_dir, "template.py"))
    shutil.copyfile(parameters_path, os.path.join(output_dir, "parameters.py"))

    # create MarsRun objects for this survey runs
    survey_tensor = SurveyTensor(output_dir, input_parameters_dict)
    runs = [_surveyutilsRunner(input_file_path=template_path,
                               run_dir=single_run.path,
                               exe_path=exe_path,
                               new_input_name="generated_input_file",
                               class_name="SurveyParameters",
                               **single_run)
            for single_run in survey_tensor]

    # create the jobs
    jobs = [run.get_job() for run in runs]

    # submit jobs
    jobsmanager.JpbsManager(jobs=jobs, work_dir=output_dir).submit.wait()

    print(f"Output directory is {output_dir!r}")
